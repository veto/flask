import config
from flask_wtf import Form

from wtforms import TextField, FileField, TextAreaField, BooleanField, SubmitField, PasswordField, validators, ValidationError, SelectField, IntegerField
from wtforms.validators import Required, Length, InputRequired, EqualTo
from flask_wtf.file import FileRequired, FileAllowed

class BaseForm(Form):
    @classmethod
    def append_field(cls, name, field):
        setattr(cls, name, field)
        return cls
    

class ContactForm(Form):
    name = TextField("Name",  [validators.Required("Please enter your name.")])
    email = TextField("Email",  [validators.Required("Please enter your email address."), validators.Email("Please enter your email address.")])
    subject = TextField("Subject",  [validators.Required("Please enter a subject.")])
    message = TextAreaField("Message",  [validators.Required("Please enter a message.")])
    submit = SubmitField("Send")

class SearchProductsForm(Form):
    search_product = TextField("Search Product", [validators.Required()])
    submit = SubmitField('Submit')
 
class CategoriesForm(Form):
    #name = TextField("Name", [InputRequired()])
    name = TextField('Name', [validators.Required()])
    submit = SubmitField("Submit")
    delete = SubmitField("Delete")

class BrandsForm(Form):
    #name = TextField("Name", [InputRequired()])
    name = TextField('Name', [validators.Required()])
    slogan = TextField('Slogan', [validators.Required()])
    description = TextAreaField('Description', [validators.Required()])
    upload = FileField('Upload Brands Image')
    display_id = IntegerField('Display ID', [validators.Required()])
    submit = SubmitField("Submit")
    delete = SubmitField("Delete")

class LoginForm(Form):
    username = TextField('Username', [validators.Required()])
    password = PasswordField('Password', [validators.Required()])
    login = SubmitField("Login")

class ProductsForm(Form):
    name = TextField('Name', [validators.Required()])
    description = TextAreaField('Description', [validators.Required()])
    upload_image = FileField('Upload Product Image')
    upload_catalogue = FileField('Upload Product Catalogue')
    upload_manual = FileField('Upload Product Manual')
    brand = SelectField(validators=[validators.optional()],choices=[('', '')])
    category = SelectField(validators=[validators.optional()],choices=[('', '')])
    display_id = IntegerField('Display ID', [validators.optional()])
    submit = SubmitField("Submit")
    delete = SubmitField("Delete")

class FeaturedBrandsForm(Form):
    #name = TextField("Name", [InputRequired()])
    brand_id = SelectField(validators=[validators.optional()],choices=[('', '')])
    upload_image = FileField('Upload Brands Image')
    display_id = IntegerField('Display ID', [validators.Required()])
    submit = SubmitField("Submit")
    delete = SubmitField("Delete")
    remove_img = SubmitField("Remove Image")

    
class FeaturedProductsForm(Form):
    brand_id = SelectField(validators=[validators.optional()],choices=[('', '')])
    display_name = TextField('Display Name', [validators.Required()])
    upload_image = FileField('Upload Featured Product Image')
    submit = SubmitField("Submit")
    delete = SubmitField("Delete")

class UpdatePasswordForm(Form):
    new_password = PasswordField('New Password', [validators.Required()])
    old_password  = PasswordField('Old Password', [validators.Required()])
    submit = SubmitField("Submit")
    delete = SubmitField("Delete")

class BaseForm(Form):
    @classmethod
    def append_field(cls, name, field):
        setattr(cls, name, field)
        return cls


