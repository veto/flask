from config import *

login_manager = flask_login.LoginManager()
login_manager.init_app(app)

# Creates empty User class with all the necessary methods that Flask-Login expects user objects to have
class User(flask_login.UserMixin):
    pass

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def page_not_found(e):
    return render_template('500.html'), 500

@login_manager.user_loader
def user_loader(user_id):
    # Check if user_id is in DB.
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Admin_users''')
    data = cur.fetchall()
    cur.close()
    for row in data:
        if user_id not in row:
            return 
        # If user_is is in DB
        # Create empty User class with all necessary methods
        user = User()
        user.id = user_id
        return user

# Front End Code
# Function for context processor
@app.context_processor
# context processor to display featured products
def display_featured_products():
    display_products = {} 
    db = database()
    cur = db.cursor()
    cur.execute("SELECT * FROM Featured_products ORDER BY display_name ASC")
    data = cur.fetchall()
    for row in data:
        display_products[row[3]] = row[1]
    display_products = collections.OrderedDict(sorted(display_products.items()))

    return dict(display_products=display_products)

@app.context_processor
def brand_links():
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Brands ORDER BY CAST(display_id AS DECIMAL(10,2)) ASC''')
    #cur.execute('''SELECT * FROM Brands ORDER BY display_id ASC''')
    data = cur.fetchall()
    cur.close()
    return dict(brand_link_data = data)

# End of function for context processor

@app.route("/")
def index():
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Featured_brands''')
    data = cur.fetchall()
    cur.close()
    return render_template('index.html', data=data)

@app.route("/contact_us")
def contact_us():
    return render_template('contact_us.html')

@app.route('/brands')
def brands():
    form = SearchProductsForm()
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Brands ORDER BY CAST(display_id AS DECIMAL(10,2)) ASC''')
    #cur.execute('''SELECT * FROM Brands ORDER BY display_id ASC''')
    data = cur.fetchall()
    cur.close()
    return render_template('brands.html', data=data, form=form)

@app.route('/products')
def products():
    form = SearchProductsForm()
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Featured_brands ORDER BY CAST(display_id AS DECIMAL(10,2)) ASC''')
    #cur.execute('''SELECT * FROM Featured_brands ORDER BY display_id ASC''')
    db.commit()
    featured_brands_data = cur.fetchall()
    cur.execute('''SELECT * FROM Brands ORDER BY name ASC''')
    brands_data = cur.fetchall()
    db.commit()
    cur.close()
    return render_template('products.html', featured_brands_data=featured_brands_data,brands_data=brands_data, form=form)

@app.route('/products_by_brands/<brand_id>')
def products_by_brands(brand_id):
    db = database()
    cur = db.cursor()
    cur.execute("SELECT * FROM Products WHERE brand = '%s' ORDER BY CAST(display_id AS DECIMAL(10,2)) ASC" %(brand_id))
    #cur.execute("SELECT * FROM Products WHERE brand = '%s' ORDER BY display_id ASC" %(brand_id))    
    product_data = cur.fetchall()
    cur.execute("SELECT * FROM Brands WHERE id = '%s'" %(brand_id))    
    brand_data = cur.fetchall()
    available_brand_cat = []
    for row in product_data:
        cat_id = row[7]
        cur.execute("SELECT * FROM Categories WHERE id = '%s'" %(cat_id))
        cat_data = cur.fetchone()
        cat_data_selected = [cat_data[0],cat_data[1]]
        available_brand_cat.append(cat_data_selected)
    cur.close()
    available_brand_cat = remove_duplicates(available_brand_cat)

    #return str(cat_data[0][1])
    return render_template('products_by_brands.html', product_data=product_data, brand_data=brand_data, available_brand_cat=available_brand_cat)

@app.route('/products_by_categories/<brand_id>/<cat_id>')
def products_by_categories(brand_id,cat_id):
    db = database()
    cur = db.cursor()
    cur.execute("SELECT * FROM Products WHERE (brand = '%s' AND category='%s' )ORDER BY category ASC" %(brand_id,cat_id))    
    product_data = cur.fetchall()
    cur.execute("SELECT * FROM Brands WHERE id = '%s'" %(brand_id))    
    brand_data = cur.fetchall()
    cur.execute("SELECT * FROM Categories WHERE id = '%s'" %(cat_id))    
    cat_data = cur.fetchall()
    cur.close()
    return render_template('products_by_categories.html',brand_data=brand_data,product_data=product_data,cat_data=cat_data)

@app.route('/products_glossary')
def products_glossary():
    db = database()
    cur = db.cursor()
    cur.execute("SELECT * FROM Products ORDER BY name ASC")    
    product_data = cur.fetchall()
    cur.execute("SELECT * FROM Brands ORDER BY name ASC")    
    brand_data = cur.fetchall()
    cur.close()
    return render_template('products_glossary.html', product_data=product_data, brand_data = brand_data)

# Front End Code

@app.route("/search_products", methods=['GET', 'POST'])
def search_products():
    form = SearchProductsForm()
    if form.validate() == True:
        search_product = form.search_product.data
        db = database()
        cur = db.cursor()
        cur.execute("SELECT * FROM Products WHERE name = '%s'" %(search_product))
        product_data = cur.fetchall()
        cur.close()
        return render_template('search_products.html', product_data=product_data, form=form)
    else:
        message = Markup("<p class='text-danger'>No such product found. <br> Please ensure you have keyed in the right name.</p>")
        flash(message)
        return redirect('brands')

@app.route("/product/<product_id>")
def product(product_id):
    db = database()
    cur = db.cursor()
    cur.execute("SELECT * FROM Products WHERE id = '%s'" %(product_id))
    product_data = cur.fetchone()
    cat_id = product_data[7]
    cur.execute("SELECT * FROM Categories WHERE id = '%s'" %(cat_id))
    cat_data = cur.fetchone()
    brand_id = product_data[8]
    cur.execute("SELECT * FROM Brands WHERE id = '%s'" %(brand_id))
    brand_data = cur.fetchone()
    cur.close()
    return render_template('product.html', product_data=product_data, cat_data=cat_data, brand_data=brand_data)
###
#@app.route('/contact', methods=['GET', 'POST'])
#def contact():
#    form = ContactForm()
#    if request.method == 'POST':
#        if form.validate() == False:
#            return render_template('contact.html', form=form)
#        else:
#            msg = Message(form.subject.data, sender='luishengjie8@gmail.com', recipients=['luishengjie8@gmail.com'])
#            msg.body = """
#            From: %s <%s>
#            %s
#            """ % (form.name.data, form.email.data, form.message.data)
#            mail.send(msg)
#            flash('Message Successfully Sent')
#            return redirect(url_for('contact'))
 
#    elif request.method == 'GET':
#        return render_template('contact.html', form=form)
###
@app.route('/about')
def about():
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Featured_brands''')
    data = cur.fetchall()
    cur.close()
    return render_template('about.html', data=data)

# End of Front End Code

# Back End Code
# When someone tries to enter a restricted page without logging in
@login_manager.unauthorized_handler
def unauthorized_handler():
    message = Markup("<p class='text-danger'>The Page that you are trying to access is restricted.<br>Please Log in to continue. </p>")
    flash(message)
    return redirect(url_for('login'))

@app.route('/login')
def login():
    form = LoginForm()
    return render_template('login.html', form=form)

@app.route('/login', methods=['POST'])
def login_admin():
    form = LoginForm()
    message = Markup("<p class='text-danger'>An Error has occured.<br>Username / Password is not valid.</p>")
    if form.validate_on_submit():
        username = form.data['username']
        password = form.data['password']
        password = password.encode('utf-8')
        db = database()
        cur = db.cursor()
        admin = list() 
        r = cur.execute("SELECT * FROM Admin_users WHERE username = '%s'" %(username))
        if cur.rowcount == 1:
            data = cur.fetchall()
            cur.close()
            for row in data:
                admin_username = row[1]
                admin_password = row[2]
                admin_password = admin_password.encode('utf-8')

            if hashpw(password, admin_password) == admin_password:
                user = User()
                user.id = username
                flask_login.login_user(user)
                return redirect(url_for('admin'))
            else:
                message = Markup("<p class='text-danger'>An Error has occured.<br>Password is not valid.</p>")
            
        else:
            cur.close()
            message = Markup("<p class='text-danger'>An Error has occured.<br>Username is not valid.</p>")
            flash(message)
            return redirect(url_for('login'))    

    flash(message)
    return redirect(url_for('login'))


@app.route('/logout')
def logout():
    flask_login.logout_user()
    message = Markup("<p class='text-success'>You have successfully logged out</p>")
    flash(message)
    return redirect(url_for('login'))

# End of Flask_Login
@app.route('/update_password', methods=['GET', 'POST'])
@login_required
def update_password():
    form = UpdatePasswordForm()
    # if it is a POST request
    if request.method == 'POST':
        if form.validate_on_submit():
            current_user=flask_login.current_user
            current_user = current_user.get_id()
            new_password = form.data['new_password']
            new_password = new_password.encode('utf-8')
            old_password = form.data['old_password']
            old_password = old_password.encode('utf-8')
            db = database()
            cur = db.cursor()
            cur.execute("SELECT * FROM Admin_users WHERE username = '%s'" %(current_user))
            data = cur.fetchall()
            cur.close()
            for row in data:
                admin_password = row[2]
                admin_password = admin_password.encode('utf-8')

            if hashpw(old_password, admin_password) == admin_password:
                hashed = hashpw(new_password, gensalt())
                cur = db.cursor()
                cur.execute("UPDATE Admin_users SET password = %s  WHERE username = %s",(hashed,str(current_user)))
                db.commit()
                db.close()
                message = "Password Successfully Updated"
                flash(message)
                return redirect(url_for('update_password'))
            else:
                message = "An Error Has Occured. Ensure All fields are filled correctly"
                flash(message)
                return redirect(url_for('update_password'))
        else:
            message = "An Error Has Occured. Ensure All fields are filled correctly"
            flash(message)
            return redirect(url_for('update_password'))
    # if it is a GET request
    else:
        return render_template('update_password.html', form=form)


@app.route('/uploads/<path:filename>')
def uploaded_img(filename):
    return send_from_directory(app.config['UPLOAD_IMAGES_FOLDER'],
                               filename, as_attachment=True)

@app.route('/upload_featured_img/<path:filename>')
def uploaded_featured_img(filename):
    return send_from_directory(app.config['UPLOAD_FEATURED_IMAGES_FOLDER'],
                               filename, as_attachment=True)

@app.route('/upload_files/<path:filename>')
# SET as_attachemnt=False - Opens file in browser
# SET as_attachemnt=True = Downloads file
def uploaded_files(filename):
    return send_from_directory(app.config['UPLOAD_FILES_FOLDER'],
                               filename, as_attachment=False)

@app.route("/admin_panel")
@login_required
def admin():
    return render_template('admin_panel.html')

# functions for Brands
# 02/07/2016
@app.route("/admin_brands", methods=['GET','POST'])
@login_required
def admin_brands():
    form = BrandsForm()
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Brands ORDER BY CAST(display_id AS DECIMAL(10,2)) ASC''')
    #cur.execute('''SELECT * FROM Brands ORDER BY display_id ASC''')
    data = cur.fetchall()
    cur.execute('''SELECT id FROM Brands ORDER BY id DESC''')
    last_id = cur.fetchone()
    for last in last_id:
        display_id = last + 1


    if request.method == 'POST':
        if form.validate_on_submit():
            brand_name = form.data['name']
            brand_slogan = form.data['slogan']
            brand_description = form.data['description']  

        if form.upload.data:
            brand_img = secure_filename(form.upload.data.filename)
            if allowed_image(brand_img):
                form.upload.data.save(app.config['UPLOAD_IMAGES_FOLDER'] + brand_img)
   
                cur.execute("INSERT INTO Brands(name, slogan, description, image, display_id) VALUES(%s, %s, %s, %s, %s)",(str(brand_name),str(brand_slogan),str(brand_description),str(brand_img),str(display_id) ))

                message = Markup("<p class='text-success'>Brand successfully added</p>")
            else:
                message = Markup("<p class='text-danger'>An Error has occured.<br>Ensure all file uploaded is an image.</p>")

        else:
            cur.execute("INSERT INTO Brands(name, slogan, description, display_id) VALUES(%s, %s, %s, %s)",(str(brand_name),str(brand_slogan),str(brand_description),str(display_id)))
            message = Markup("<p class='text-success'>Brand successfully added</p>")            

        db.commit()
        db.close()
        cur.close()
        flash(message)
        
        return redirect(url_for('admin_brands'))        
    form.display_id.data = display_id
    return render_template('admin_brands.html', data=data, form=form) 


@app.route("/update_brands/<xx>", methods=['GET','POST'])
@login_required
def update_brands(xx):
    form = BrandsForm()
    db = database()
    cur = db.cursor()
    cur.execute("SELECT * FROM Brands WHERE id = '%s'" %(xx))
    data = cur.fetchall()
    cur.close()
    cur = db.cursor()
    cur.execute("SELECT COUNT(*) FROM Brands;")
    data2 = cur.fetchall()
    

    if request.method == 'POST':
        if form.validate_on_submit():
            brand_name = form.data['name']
            brand_slogan = form.data['slogan']
            brand_description = form.data['description']
            display_id = form.data['display_id']
            db = database()
            cur = db.cursor()
            if form.submit.data:
                if form.upload.data:
                    brand_img = secure_filename(form.upload.data.filename)
                    if allowed_image(brand_img):    
                        cur.execute("SELECT image FROM Brands WHERE id = '%s'" %(xx))
                        old_brand_image = cur.fetchone()
                        old_brand_image = ''.join(old_brand_image)
                        if os.path.isfile(old_brand_image):
                            os.remove(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], old_brand_image))
                        form.upload.data.save(app.config['UPLOAD_IMAGES_FOLDER'] + brand_img)
                        cur.execute("UPDATE Brands SET name = %s , slogan = %s , description = %s , image = %s, display_id = %s WHERE id = %s",(str(brand_name),str(brand_slogan),str(brand_description),str(brand_img),str(display_id),str(xx)))
                    else:
                        message = Markup("<p class='text-danger'> An Error Has Occured<br>"+(str(brand_name))+" uploaded image is not valid</p>")
                        flash(message)
                        return redirect(url_for('updated_brands', xx=xx))
                        

                else:
                    cur.execute("UPDATE Brands SET name = %s , slogan = %s , description = %s, display_id = %s WHERE id = %s",(str(brand_name),str(brand_slogan),str(brand_description),str(display_id),str(xx)))
                db.commit()
                db.close()
                message = Markup("<p class='text-success'>"+(str(brand_name))+" has been succesfully updated</p>")
                
            else: 
                cur.execute("DELETE from Brands WHERE id = %s",(str(xx)))
                db.commit()
                db.close()
                message = Markup("<p class='text-success'>"+(str(brand_name))+" has been succesfully Deleted</p>")

        else:        
            message = Markup("<p class='text-danger'>Error Occured. Brand not updated</p>")

        flash(message)
        return redirect(url_for('admin_brands'))

    for row in data:
        brand_name = row[1]
        brand_slogan = row[2]
        brand_description = row[3]
        brand_image = row[5]
        display_id = row[6]

    form.name.data = brand_name
    form.slogan.data = brand_slogan
    form.description.data = brand_description
    form.upload.data = brand_image
    form.display_id.data = display_id

    return render_template('update_brands.html', data=data, data2=data2, form=form)


# End of function for Brands

# function for Categories
@app.route("/admin_categories")
@login_required
def admin_categories():
    form = CategoriesForm()
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Categories''')
    data = cur.fetchall()
    cur.close()
    return render_template('admin_categories.html', data=data, form=form)   

@app.route('/admin_categories', methods=['POST'])
@login_required
def insert_category():
    form = CategoriesForm()
    if form.validate_on_submit():
        cat_name = form.data['name']
        # Upload data into MySQL Database
        db = database()
        cur = db.cursor()
        cur.execute("INSERT INTO Categories(name) VALUES(%s)",(str(cat_name)))
        db.commit()
        db.close()
        message = Markup("<p class='text-success'>Category successfully added</p>")
        flash(message)
        return redirect(url_for('admin_categories'))
    else:
        message = Markup("<p class='text-danger'>Enter Category Name</p>")
        flash(message)
        return redirect(url_for('admin_categories'))

@app.route("/update_categories/<xx>")
@login_required
def update_categories(xx):
    form = CategoriesForm()
    db = database()
    cur = db.cursor()
    cur.execute("SELECT * FROM Categories WHERE id = '%s'" %(xx))
    data = cur.fetchall()
    cur.execute("SELECT * FROM Categories")
    data2 = cur.fetchall()
    cur.close()
    for row in data:
        cat_name = row[1]
    form.name.data = cat_name
    return render_template('update_categories.html', data=data, data2=data2, form=form)

@app.route('/update_categories/<xx>', methods=['POST'])
@login_required
def updated_categories(xx):
    form = CategoriesForm()
    if form.validate_on_submit():
        cat_name = form.data['name']
        db = database()
        cur = db.cursor()
        if form.submit.data:
            cur.execute("UPDATE Categories SET name = %s WHERE id = %s",(str(cat_name),str(xx)))
            db.commit()
            db.close()
            message = Markup("<p class='text-success'>"+(str(cat_name))+" has been succesfully updated</p>")
            flash(message)
            return redirect(url_for('admin_categories'))
        else: 
            cur.execute("DELETE from Categories WHERE id = %s",(str(xx)))
            db.commit()
            db.close()
            message = Markup("<p class='text-success'>"+(str(cat_name))+" has been succesfully Deleted</p>")
            flash(message)
            return redirect(url_for('admin_categories'))            
    else:
        message = Markup("<p class='text-danger'>Error Occured. Category not updated</p>")
        flash(message)
        return redirect(url_for('admin_categories'))
 
# End of functions for Categories

# functions for Products
def cat_choices():
    cat_choices =[]
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Categories''')
    cat_data = cur.fetchall()
    cur.close()
    for row in cat_data:
        cat_choices.append((str(row[0]),str(row[1])))
    return cat_choices

def brand_choices():
    brand_choices =[]
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Brands''')
    brand_data = cur.fetchall()
    cur.close()
    for row in brand_data:
        brand_choices.append((str(row[0]),str(row[1])))
    return brand_choices

def featured_brand_choices():
    featured_brand_choices =[]
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Featured_brands''')
    featured_brand_data = cur.fetchall()
    cur.close()
    for row in featured_brand_data:
        featured_brand_choices.append((str(row[0]),str(row[1])))
    return featured_brand_choices

# Function for featured_brands
# 10/7/2016
@app.route("/admin_products",  methods=['POST','GET'])
@login_required
def admin_products():
    c = cat_choices()
    b = brand_choices()
    form = ProductsForm()
    form.category.choices = c
    form.brand.choices = b
    db = database()
    cur = db.cursor()
    cur.execute("SELECT * FROM Brands ")    
    brand_data = cur.fetchall()
    cur.execute('''SELECT * FROM Products ORDER BY CAST(display_id AS DECIMAL(10,2)) ASC''')
    data = cur.fetchall()
    cur.execute('''SELECT COUNT(*) FROM Products;''')
    last_id = cur.fetchone()
    for last in last_id:
        display_id = last + 1

    if request.method == 'POST':
        if form.validate_on_submit():
            if form.upload_image.data:
                product_name = form.data['name']
                product_description = form.data['description']
                product_category = form.data['category']
                product_brand = form.data['brand']
                display_id = form.data['display_id']

                product_img = secure_filename(form.upload_image.data.filename)
                product_catalogue = ""
                if form.upload_catalogue.data:
                    product_catalogue = secure_filename(form.upload_catalogue.data.filename)
                product_manual = ""
                if form.upload_manual.data:
                    product_manual = secure_filename(form.upload_manual.data.filename)
                
                if allowed_image(product_img):
                    form.upload_image.data.save(app.config['UPLOAD_IMAGES_FOLDER'] +product_img)
                    # Insert Values into db
                    db = database()
                    cur = db.cursor()
                    # If Products Catalogue exists
                    if allowed_file(product_catalogue):
                        form.upload_catalogue.data.save(os.path.join(app.config['UPLOAD_FILES_FOLDER'], product_catalogue))
                        #form.upload_catalogue.data.save(app.config['UPLOAD_FILES_FOLDER'] +product_catalogue)
                    # If Products Manual exists
                    if allowed_file(product_manual):
                        form.upload_manual.data.save(os.path.join(app.config['UPLOAD_FILES_FOLDER'], product_manual))
                    
                    cur.execute("INSERT INTO Products(name, description, image, category, brand, catalogue, manual, display_id) VALUES(%s, %s, %s, %s, %s, %s, %s, %s)",(str(product_name),str(product_description),str(product_img),str(product_category),str(product_brand),str(product_catalogue),str(product_manual),str(display_id)))
                    db.commit()
                    db.close()
                    message = Markup("<p class='text-success'>Product successfully added</p>")
                    
                else:
                    message = Markup("<p class='text-danger'>An Error has occured.<br>Ensure all data uploaded are in the correct format.</p>")
            else:
                message = Markup("<p class='text-danger'>An Error has occured.<br>Ensure all fields have been filled.</p>")

            flash(message)
            return redirect(url_for('admin_products'))

    form.display_id.data = display_id
    return render_template('admin_products.html', data = data, form = form, brand_data = brand_data)   

            

@app.route('/update_products/<xx>', methods=['POST', 'GET'])
@login_required
def update_products(xx):
    c = cat_choices()
    b = brand_choices()
    form = ProductsForm()
    form.category.choices = c
    form.brand.choices = b
    db = database()
    cur = db.cursor()
    cur.execute("SELECT * FROM Brands ")    
    brand_data = cur.fetchall()
    cur.execute("SELECT * FROM Products WHERE id = '%s'" %(xx))
    data = cur.fetchall()
    cur.execute("SELECT * FROM Products ORDER BY CAST(display_id AS DECIMAL(10,2)) ASC")
    data2 = cur.fetchall()
    cur.close()
    for row in data:
        product_name = row[1]
        product_description = row[2]
        product_img = row[4]
        product_catalogue = row[5]
        product_manual = row[6]
        product_category = row[7]
        product_brand = row[8]
        display_id = row[9]

    if request.method == 'POST':
        if form.validate_on_submit():
            db = database()
            cur = db.cursor()
            if form.submit.data:
                product_name = form.data['name']
                product_description = form.data['description']
                product_category = form.data['category']
                product_brand = form.data['brand']
                display_id = form.data['display_id']
                if form.upload_image.data:
                    product_img = secure_filename(form.upload_image.data.filename)
                    
                    if allowed_image(product_img):
                        cur.execute("SELECT image FROM Products WHERE id = '%s'" %(xx))
                        old_product_image = cur.fetchone()
                        old_product_image = ''.join(old_product_image)
                        if os.path.isfile(old_product_image):
                            os.remove(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], old_brand_image))
                        form.upload_image.data.save(app.config['UPLOAD_IMAGES_FOLDER'] +product_img)

                if form.upload_catalogue.data:
                    product_catalogue = secure_filename(form.upload_catalogue.data.filename)
                    
                    if allowed_file(product_catalogue):
                        cur.execute("SELECT catalogue FROM Products WHERE id = '%s'" %(xx))
                        old_product_catalogue = cur.fetchone()
                        old_product_catalogue = ''.join(old_product_catalogue)
                        if os.path.isfile(old_product_catalogue):
                            os.remove(os.path.join(app.config['UPLOAD_FILES_FOLDER'], old_product_catalogue))
                        form.upload_catalogue.data.save(os.path.join(app.config['UPLOAD_FILES_FOLDER'], product_catalogue))


                if form.upload_manual.data:
                    product_manual = secure_filename(form.upload_manual.data.filename)
                    
                    if allowed_file(product_manual):
                        cur.execute("SELECT manual FROM Products WHERE id = '%s'" %(xx))
                        old_product_manual = cur.fetchone()
                        old_product_manual = ''.join(old_product_manual)
                        if os.path.isfile(old_product_manual):
                            os.remove(os.path.join(app.config['UPLOAD_FILES_FOLDER'], old_product_manual))
                        form.upload_manual.data.save(os.path.join(app.config['UPLOAD_FILES_FOLDER'], product_manual))
                        

                cur.execute("UPDATE Products SET name = %s , description = %s , image = %s , category = %s, brand = %s , catalogue = %s, manual = %s, display_id = %s WHERE id = %s",(str(product_name),str(product_description),str(product_img),str(product_category),str(product_brand),str(product_catalogue),str(product_manual),str(display_id),str(xx)))
                message = Markup("<p class='text-success'>Product successfully updated</p>")
                db.commit()
                db.close()



            else:
                cur.execute("DELETE from Products WHERE id = %s",(str(xx)))
                message = Markup("<p class='text-success'>"+(str(product_name))+" has been succesfully Deleted</p>")
                db.commit()
                db.close()
                
        else:
            message = Markup("<p class='text-danger'>Error Occured. Product not updated</p>")
        
        flash(message)
        return redirect(url_for('admin_products'))







    # SET THE DEFAULT VALUES
    form.name.data = product_name
    form.description.data = product_description
    #form.upload_image.data = product_img
    form.upload_catalogue.data = product_catalogue
    form.upload_manual.data = product_manual
    form.category.data = product_category
    form.brand.data = product_brand
    form.display_id.data = display_id
    return render_template('update_products.html', data=data, data2=data2, form=form, brand_data = brand_data)   



# Function for featured_brands
# 28/6/2016
@app.route('/admin_featured_brands', methods=['POST','GET'])
@login_required
def admin_featured_brands():
    b = brand_choices()
    form = FeaturedBrandsForm()
    form.brand_id.choices = b
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Featured_brands ORDER BY CAST(display_id AS DECIMAL(10,2)) ASC''')
    
    #cur.execute('''SELECT * FROM Featured_brands ORDER BY display_id ASC''')
    data = cur.fetchall()
    cur.execute('''SELECT COUNT(*) FROM Featured_brands;''')
    last_id = cur.fetchone()
    for last in last_id:
        display_id = last + 1

    if request.method == 'POST':
        if form.validate_on_submit():
            brand_id = form.data['brand_id']
            
            query = cur.execute("SELECT name From Brands Where id ='%s'" %(brand_id))
            query = cur.fetchone()
            brand_name = query[0]
            # Upload Featured Brand Image
            if form.upload_image.data:
                featured_brand_image = secure_filename('featured_brand_'+str(random_with_N_digits(4))+'_'+form.upload_image.data.filename)
                if allowed_image(featured_brand_image):
                    form.upload_image.data.save(app.config['UPLOAD_IMAGES_FOLDER'] +featured_brand_image)
                    cur.execute("INSERT INTO Featured_brands(name,brand,brand_image,display_id) VALUES(%s,%s,%s,%s)",(str(brand_name), str(brand_id),str(featured_brand_image),str(display_id)))
                    message = Markup("<p class='text-success'>Featured Brand successfully added</p>")
                else:
                    message = Markup("<p class='text-danger'>Error! Featured Product Brand not in the right format</p>")
            # End of Upload Featured Product Image
            
            else:
                cur.execute("INSERT INTO Featured_brands(name,brand,display_id) VALUES(%s,%s,%s)",(str(brand_name),str(brand_id),str(display_id)))
                message = Markup("<p class='text-success'>Featured Product successfully added</p>")
            
            db.commit()
            db.close()
            cur.close()
            
            flash(message)
            return redirect(url_for('admin_featured_brands'))

    form.display_id.data = display_id
    return render_template('admin_featured_brands.html', form=form, data = data)

@app.route('/update_featured_brands/<xx>', methods=['POST','GET'])
@login_required
def update_featured_brands(xx):
    b = brand_choices()
    form = FeaturedBrandsForm()
    form.brand_id.choices = b
    db = database()
    cur = db.cursor()
    cur.execute("SELECT * FROM Featured_brands WHERE id = '%s'" %(xx))
    data = cur.fetchall()
    cur.execute('''SELECT * FROM Featured_brands ORDER BY CAST(display_id AS DECIMAL(10,2)) ASC''')
    #cur.execute("SELECT * FROM Featured_brands ORDER BY display_id ASC")
    data2 = cur.fetchall()
    for row in data:
        brand_id = row[3]
        brand_img = row[4]
        display_id = row[5]

    if request.method == 'POST':
        if form.validate_on_submit():
            if form.submit.data:
                brand_id = form.brand_id.data
                display_id = form.display_id.data
                cur.execute("SELECT name From Brands Where id ='%s'" %(brand_id))
                query = cur.fetchone()
                brand_name = query[0]
                if form.upload_image.data:
                    featured_brand_image = secure_filename('featured_brand_'+str(random_with_N_digits(4))+'_'+form.upload_image.data.filename)
                    if brand_img != None:
                        if os.path.isfile(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], brand_img)):
                            os.remove(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], brand_img))

                    if allowed_image(featured_brand_image):
                        form.upload_image.data.save(app.config['UPLOAD_IMAGES_FOLDER'] +featured_brand_image)
                        cur.execute("UPDATE Featured_brands SET name = %s, brand = %s, brand_image=%s, display_id=%s WHERE id = %s",(str(brand_name),str(brand_id),str(featured_brand_image),str(display_id),str(xx)))
                else:
                    cur.execute("UPDATE Featured_brands SET name = %s, brand = %s, display_id = %s WHERE id = %s",(str(brand_name),str(brand_id),str(display_id),str(xx)))
                
                    
                db.commit()
                db.close()
                cur.close()
                message = Markup("<p class='text-success'>"+(str(brand_name))+" has been succesfully updated</p>")
                flash(message)
                return redirect(url_for('admin_featured_brands'))
            
            elif form.remove_img.data :
            # Delete Featured product image
                if brand_img != None:
                    if os.path.isfile(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], brand_img)):
                        os.remove(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], brand_img))
                    empty_brand_img = None
                    cur.execute("UPDATE Featured_brands SET brand_image = %s WHERE id = %s",(empty_brand_img,str(xx)))
                    db.commit()
                    db.close()
                    cur.close()
                return redirect(url_for('update_featured_brands',xx=xx))

            else:

                if brand_img != None:
                # Delete Featured product image
                    if os.path.isfile(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], brand_img)):
                        os.remove(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], brand_img))

                # Delete the featured product entry
                query = cur.execute("SELECT name FROM Featured_brands WHERE id = %s",(str(xx)))
                query = cur.fetchone()
                delete_brand_name = query[0]
                cur.execute("DELETE from Featured_brands WHERE id = %s",(str(xx)))
                db.commit()
                db.close()
                cur.close()
                message = Markup("<p class='text-danger'>"+(str(delete_brand_name))+" has been succesfully Deleted</p>")
                flash(message)
                return redirect(url_for('admin_featured_brands'))        

    # SET THE DEFAULT VALUE 
    form.brand_id.data = brand_id
    form.display_id.data = display_id
    return render_template('update_featured_brands.html', form=form, data=data, data2=data2)

# End of functions for featured products


# Function for featured_products
# 23/6/2016
@app.route('/admin_featured_products', methods=['POST','GET'])
@login_required
def admin_featured_products():
    b = brand_choices()
    form = FeaturedProductsForm()
    form.brand_id.choices = b
    db = database()
    cur = db.cursor()
    cur.execute('''SELECT * FROM Featured_products''')
    data = cur.fetchall()

    if request.method == 'POST':
        if form.validate_on_submit():
            brand_id = form.data['brand_id']
            display_name = form.data['display_name']

            query = cur.execute("SELECT name From Brands Where id ='%s'" %(brand_id))
            query = cur.fetchone()
            brand_name = query[0]
            # Upload Featured Product Image
            if form.upload_image.data:
                featured_product_img = secure_filename('featured_product_'+form.upload_image.data.filename)
                if allowed_image(featured_product_img):
                    form.upload_image.data.save(app.config['UPLOAD_IMAGES_FOLDER'] +featured_product_img)
                    cur.execute("INSERT INTO Featured_products(display_name,brand_name,brand_id,product_image) VALUES(%s,%s,%s,%s)",(str(display_name), str(brand_name),str(brand_id),str(featured_product_img)))
                    message = Markup("<p class='text-success'>Featured Product successfully added</p>")
                else:
                    message = Markup("<p class='text-danger'>Error! Featured Product Image not in the right format</p>")
            # End of Upload Featured Product Image
            else:
                cur.execute("INSERT INTO Featured_products(display_name,brand_name,brand_id) VALUES(%s,%s,%s)",(str(display_name), str(brand_name),str(brand_id)))
                message = Markup("<p class='text-success'>Featured Product successfully added</p>")
            
            db.commit()
            db.close()
            cur.close()
            
            flash(message)
            return redirect(url_for('admin_featured_products'))

    return render_template('admin_featured_products.html', form=form, data = data)

@app.route('/update_featured_products/<xx>', methods=['POST','GET'])
@login_required
def update_featured_products(xx):
    b = brand_choices()
    form = FeaturedProductsForm()
    form.brand_id.choices = b
    db = database()
    cur = db.cursor()
    cur.execute("SELECT * FROM Featured_products WHERE id = '%s'" %(xx))
    data = cur.fetchall()
    cur.execute("SELECT * FROM Featured_products")
    data2 = cur.fetchall()
    for row in data:
        brand_id = row[1]
        display_name = row[3]
        product_img = row[4]

    if request.method == 'POST':
        if form.validate_on_submit():
            if form.submit.data:
                brand_id = form.brand_id.data
                display_name = form.display_name.data
                cur.execute("SELECT name From Brands Where id ='%s'" %(brand_id))
                query = cur.fetchone()
                brand_name = query[0]
                if form.upload_image.data:
                    if product_img != None:
                        if os.path.isfile(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], product_img)):
                            os.remove(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], product_img))

                    featured_product_img = secure_filename('featured_product_'+form.upload_image.data.filename)
                    if allowed_image(featured_product_img):
                        form.upload_image.data.save(app.config['UPLOAD_IMAGES_FOLDER'] +featured_product_img)
                        cur.execute("UPDATE Featured_products SET brand_name = %s, brand_id = %s, display_name = %s, product_image=%s WHERE id = %s",(str(brand_name),str(brand_id),str(display_name),str(featured_product_img),str(xx)))

                cur.execute("UPDATE Featured_products SET brand_name = %s, brand_id = %s, display_name = %s WHERE id = %s",(str(brand_name),str(brand_id),str(display_name),str(xx)))
                db.commit()
                db.close()
                cur.close()
                message = Markup("<p class='text-success'>"+(str(display_name))+" has been succesfully updated</p>")
                flash(message)
                return redirect(url_for('admin_featured_products'))
                
            else:
                if product_img != None:
                # Delete Featured product image
                    if os.path.isfile(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], product_img)):
                        os.remove(os.path.join(app.config['UPLOAD_IMAGES_FOLDER'], product_img))

                # Delete the featured product entry
                query = cur.execute("SELECT display_name FROM Featured_products WHERE id = %s",(str(xx)))
                query = cur.fetchone()
                display_name = query[0]
                cur.execute("DELETE from Featured_products WHERE id = %s",(str(xx)))
                db.commit()
                db.close()
                cur.close()
                message = Markup("<p class='text-danger'>"+(str(display_name))+" has been succesfully Deleted</p>")
                flash(message)
                return redirect(url_for('admin_featured_products'))        

    # SET THE DEFAULT VALUE 
    form.brand_id.data = brand_id
    form.display_name.data = display_name
    return render_template('update_featured_products.html', form=form, data=data, data2=data2)

# End of functions for featured products

# End of Back End Code

if __name__ == "__main__":
    app.secret_key = secret_key();
    # Set debug=True when developing
    app.run(host='0.0.0.0')
