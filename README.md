# flask
a flask presentation website for addax

## how to run 
1.
```python
python3  -m venv env
```
2.
```bash
source env/bin/activate
```

3.
```python
pip install -r requirements.txt
```
